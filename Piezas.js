//Este archivo define los constructores para cada posible pieza de tetris. O sea, los gameobjects
//de nuestro juego. Definimos estas piezas en base a que el mundo de Tetris está formado por arrays
//de dos dimensiones. Creamos un constructor para cada pieza en lugar de un constructor genérico
//porque si lo hicieramos genérico tendríamos que asignar valores cada vez que creamos un objeto
//desde el engine, y creemos que así es más sencillo.

function PiezaL()
{
	//Definimos los posibles estados de la pieza utilizando arrays.
	this.estado1 = [ [1, 0],
					 [1, 0],
					 [1, 1] ];
					
	this.estado2 = [ [0, 0, 1],
					 [1, 1, 1] ];
					
	this.estado3 = [ [1, 1],
					 [0, 1],
					 [0, 1] ];
	
	this.estado4 = [ [1, 1, 1],
					 [1, 0, 0] ];
					 
	//Creamos una propiedad con la lista de posibles estados
	this.estados = [ this.estado1, this.estado2, this.estado3, this.estado4 ];
	
	//Creamos una propiedad que define la posición actual.
	this.estadoActual = 0;
	
	//Creamos una propiedad que definirá el color del que será la pieza
	this.color = 0;
	
	//Inicializamos una propiedad position, que es un objeto con dos propiedades: La x y la y, 
	//y les damos un valor inicial.
	this.posicionX = 4;
	this.posicionY = -3;
	
}

function PiezaLInvertida()
{
	//Definimos los posibles estados de la pieza utilizando arrays.
	this.estado1 = [ [0, 1],
					 [0, 1],
					 [1, 1] ];
					
	this.estado2 = [ [1, 1, 1],
					 [0, 0, 1] ];
					
	this.estado3 = [ [1, 1],
					 [1, 0],
					 [1, 0] ];
	
	this.estado4 = [ [1, 0, 0],
					 [1, 1, 1] ];
					 
	//Creamos una propiedad con la lista de posibles estados
	this.estados = [ this.estado1, this.estado2, this.estado3, this.estado4 ];
	
	//Creamos una propiedad que define la posición actual.
	this.estadoActual = 0;
	
	//Creamos una propiedad que definirá el color del que será la pieza
	this.color = 0;
	
	//Inicializamos una propiedad position, que es un objeto con dos propiedades: La x y la y, 
	//y les damos un valor inicial.
	this.posicionX = 4;
	this.posicionY = -3;
	
}

function PiezaBloque()
{
	this.estado1 = [ [1, 1],
					 [1, 1] ];
					 
	this.estados = [this.estado1];
	this.estadoActual = 0;
	
	this.color = 0;
	
	this.posicionX = 4;
	this.posicionY = -2;
}

function PiezaLinea()
{
	this.estado1 = [[1],
					[1],
					[1],
					[1]];
	this.estado2 = [ [1,1,1,1] ];
	this.estados = [ this.estado1, this.estado2 ];
	this.estadoActual = 0;
	
	this.color = 0;
	
	this.posicionX = 5;
	this.posicionY = -4;
}

function PiezaT()
{
	this.estado1 = [ [1, 1, 1],
					 [0, 1, 0] ];
					
	this.estado2 = [ [1, 0],
					 [1, 1],
					 [1, 0] ];
	
	this.estado3 = [ [0, 1, 0],
					 [1, 1, 1] ];
					
	this.estado4 = [ [0, 1],
					 [1, 1],
					 [0, 1] ];
					
	this.estados = [ this.estado1, this.estado2, this.estado3, this.estado4 ];
	this.estadoActual = 0;
	
	this.color = 0;
	
	this.posicionX = 4;
	this.posicionY = -2;
}

function PiezaZ()
{
	this.estado1 = [ [1, 1, 0],
					 [0, 1, 1] ];
					
	this.estado2 = [ [0, 1],
					 [1, 1],
					 [1, 0] ];
					
	this.estados = [ this.estado1, this.estado2 ];
	this.estadoActual = 0;
	
	this.color = 0;
	
	this.posicionX = 4;
	this.posicionY = -2;
}

function PiezaZInvertida()
{
	this.estado1 = [ [0, 1, 1],
					 [1, 1, 0] ];
					
	this.estado2 = [ [1, 0],
					 [1, 1],
					 [0, 1] ];
					
	this.estados = [ this.estado1, this.estado2 ];
	this.estadoActual = 0;
	
	this.color = 0;
	
	this.posicionX = 4;
	this.posicionY = -2;
}


//Finalmente definimos una funcion para obtener una pieza aleatoria. Debatimos sobre
//en que archivo ponerla, por conveniencia la acabamos poniendo en este en lugar de
//en el del motor ya que aquí las piezas ya están definidas.
function PiezaAleatoria()
{
	var resultado = Math.floor( Math.random() * 7 );
	var Pieza;
	
	switch(resultado)
	{
		case 0: Pieza = new PiezaL();			break;
		case 1: Pieza = new PiezaBloque();		break;
		case 2: Pieza = new PiezaZ();			break;
		case 3: Pieza = new PiezaT();			break;
		case 4: Pieza = new PiezaLInvertida();	break;
		case 5: Pieza = new PiezaZInvertida();	break;
		case 6: Pieza = new PiezaLinea();		break;
	}	
	
	Pieza.color = Math.floor(Math.random() * 8);
	return Pieza;
}