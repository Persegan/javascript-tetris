//En este archivo creamos el motor del juego.
var motor = 
{
	//Declaramos propiedades que determinan la resolucion del area del juego, y otras cosas, y nos ayudarán a situar
	//las piezas.
	filas: 20,
	columnas : 10,
	resolucion : 32,
	canvas:document.getElementById('CanvasJuego'),
	ctx:document.getElementById('CanvasJuego').getContext('2d'),
	TiempoActual:0,
	incrementotiempo:20,
	GameOver:false,
	objetolineas:document.getElementById('lineas'),
	numerolineas:0,
	time:new Date(),
	audio: new Audio('tetris.mp3')
	
}

//Creamos una funcion iniciar que llamaremos desde el html para iniciar el juego.
function Iniciar()
{
	//inicializamos valores para nuestra variable motor, cargamos las imagenes que usaremos y hacemos que al cargar las imagenes se llame a una funcion que definimos luego. También iniciamos la música.
	motor.audio.play();
	motor.cargadordeimagenes = new CargadorImagenes();
	motor.cargadordeimagenes.agregarImagen("bloques.png", "bloques");
	motor.cargadordeimagenes.agregarImagen("fondo.png", "fondo");
	motor.cargadordeimagenes.agregarImagen("gameover.png", "gameover");
	motor.cargadordeimagenes.cargarImagenes();
	motor.cargadordeimagenes.alCompletarLlamara = alImagenesCargadas();	
}

//Añadimos un eventListener para controlar las piezas
window.addEventListener("keydown", getInput, false);

function getInput(e)
{

	//Este trozo de código de este primer if no es nuestro, lo hemos puesto porque en un tutorial se recomendaba hacerlo, y no estamos muy seguros de como funciona.
	if (!e) {var e = window.event;}
	e.preventDefault();
	
	if (motor.GameOver != true)
	{
		switch (e.keyCode)
		{
			case 37:
			{
				if (detectarmovimiento(motor.piezactual.posicionX-1, motor.piezactual.posicionY, motor.piezactual.estadoActual))
				{
					motor.piezactual.posicionX --;
				}
			}
			break;
			
			case 39:
			{
				if (detectarmovimiento(motor.piezactual.posicionX+1, motor.piezactual.posicionY, motor.piezactual.estadoActual))
				{
					motor.piezactual.posicionX ++;
				}
			}
			break;
			case 38:
			{
				var estadonuevo = motor.piezactual.estadoActual-1;
				if (estadonuevo <0)
				{
					estadonuevo = motor.piezactual.estados.length-1;
				}
				if (detectarmovimiento(motor.piezactual.posicionX, motor.piezactual.posicionY, estadonuevo))
				{
					motor.piezactual.estadoActual = estadonuevo;
				}
			}
			break;
			case 40:
			{
				if (detectarmovimiento(motor.piezactual.posicionX, motor.piezactual.posicionY+1, motor.piezactual.estadoActual))
				{
					motor.piezactual.posicionY ++;
				}
			}
			break;
		}
	}
	else
	{
		inicializarjuego();
	}
	
}


//Creamos una función que coloca las imagenes cargadas, una vez cargadas, dentro de nuestro objeto motor, y ejecuta una función que hace que comienze el juego. 
function alImagenesCargadas()
{
	motor.imagenbloques = motor.cargadordeimagenes.CogerImagenEnIndice(0);
	motor.background = motor.cargadordeimagenes.CogerImagenEnIndice(1);
	motor.gameoverimagen = motor.cargadordeimagenes.CogerImagenEnIndice(2);
	
	inicializarjuego();
}


//La siguiente funcion se ejecuta cuando el juego comienza, ya sea porque es la primera vez que carga o después de un game over.
function inicializarjuego()
{
	motor.numerolineas = 0;
	motor.GameOver = false;
	motor.incrementotiempo = 20;
	
	//creamos este if para la primera iteracion de la funcion, cuando aun no hay datos en el tablero de juego. Esto crea los datos de todo el tablero y les asigna un valor de 0.
	if (motor.tablero == undefined)
	{
		motor.tablero = [];
		
		for (var f = 0; f < motor.filas; f++)
		{
			motor.tablero[f] = [];
			for (var c = 0; c < motor.columnas; c++)
			{
				motor.tablero[f].push(0);
			}
		}
	}
	//este else hace lo mismo que el primer if, pero en el caso de que ya haya datos introducidos en el tablero. Básicamente este se ejecuta cuando hay un GameOver para iniciar el juego de nuevo.
	else
	{
		for (var f = 0; f < motor.filas; f++)
		{
			for (var c = 0; c < motor.columnas; c++)
			{
				motor.tablero[f][c] = 0;
			}
		}
	}
	
	//llamamos a una primera pieza que caerá en el juego.
	motor.piezactual = PiezaAleatoria();
	motor.objetolineas.innerHTML = motor.numerolineas.toString();
	
	
	//iniciamos el bucle del juego.
	main();
	
}



//Esta funcion es una version modificada de dibujarPieza, situada más abajo. Esta dibuja la pieza "permanentemente" cuando llega al fondo.
function copiarDatos(piezactual)
{
	var posicionX = piezactual.posicionX;
	var posicionY = piezactual.posicionY;
	var estado = piezactual.estadoActual;
	
	for (var f = 0, longitud = piezactual.estados[estado].length; f < longitud; f++ )
	{
				for (var c = 0, longitud2 = piezactual.estados[estado][f].length; c < longitud2; c++)
				{
					
					if (piezactual.estados[estado][f][c] == 1 && posicionY >= 0)
					{
						motor.tablero[posicionY][posicionX] = (piezactual.color + 1);				
					}
					
					posicionX +=1;
					
				}
				posicionX = piezactual.posicionX;
				posicionY += 1;
	}
	revisarlineas();
	if (piezactual.posicionY < 0)
	{
		motor.GameOver = true;
	}
}

//Esta funcion revisa el estado del tablero den busca de lineas completadas.
function revisarlineas()
{
	var lineaencontrada = false;
	var filallena = true;
	var f = motor.filas - 1;
	var c = motor.columnas - 1;
	
	while ( f >= 0)
	{
		while ( c >= 0 )
		{
			if (motor.tablero[f][c] == 0)
			{
				filallena= false;
				c = -1;
			}
			c--;
		}
		if (filallena == true)
		{
			VaciarFila(f);
			f++;
			lineaencontrada = true;
			motor.numerolineas++;
			
		}
		
		filallena = true;
		c = motor.columnas-1;
		f--;
	}
	if (lineaencontrada)
	{
		motor.objetolineas.innerHTML = motor.numerolineas.toString();
	}
}

//Esta funcion copia los datos de una fila por encima suyo a la fila de abajo en el caso de que la fila objetivo esté llena, creando la ilusión de que estamos haciendo desaparecer una fila.
function VaciarFila (fila)
{
	var f = fila;
	var c = 0;
	
	while (f >= 0)
	{
		while (c < motor.columnas)
		{
			if (f > 0)
			{
				motor.tablero[f][c] = motor.tablero[f-1][c];
			}
			else
			{
				motor.tablero[f][c] = 0;
			}
			c++;
		}
		c = 0;
		f--;
	}
}



// La siguiente función actualiza el dibujo del tablero, incluyendo el añadir la imagen de backgrounds, y se llama desde la función actualizar.
function dibujarTablero()
{

		motor.ctx.drawImage(motor.background, 0, 0, 320, 640, 0, 0, 320, 640);
		for (var f = 0; f < motor.filas; f++)
		{
			for (var c = 0; c < motor.columnas; c++)
			{
				//miramos las posiciones en las que tiene que haber un trozo de pieza dibujada, y la dibujamos. Este trozo se encarga de las piezas estáticas, o sea, las que ya han caído.
				if (motor.tablero [f][c] != 0)
				{
					motor.ctx.drawImage(motor.imagenbloques, (motor.tablero[f][c] - 1)* motor.resolucion, 0, motor.resolucion, motor.resolucion, c* motor.resolucion, f * motor.resolucion, motor.resolucion, motor.resolucion);
					
				}
				
			}
			
		}
	
}


//Esta funcion se encarga de dibujar la pieza que está controlando el jugador en cada momento.
function dibujarPieza(piezactual)
{
	//guardamos algunas variables de la pieza en variables propias. No es estrictamente necesario, pero es para hacer el código más corto en los bucles siguientes, y además luego cambiamos el valor de una de ellas y necesitaremos volver al valor inicial.
	var dibujarX = piezactual.posicionX;
	var dibujarY = piezactual.posicionY;
	var estado = piezactual.estadoActual;
	
	for (var f = 0, longitud = piezactual.estados[estado].length; f < longitud; f++ )
	{
				for (var c = 0, longitud2 = piezactual.estados[estado][f].length; c < longitud2; c++)
				{
					//Dibujamos las zonas pertinentes del array para formar la pieza
					if (piezactual.estados[estado][f][c] == 1 && dibujarY >= 0)
					{
						motor.ctx.drawImage(motor.imagenbloques, piezactual.color * motor.resolucion, 0, motor.resolucion, motor.resolucion, dibujarX*motor.resolucion, dibujarY*motor.resolucion, motor.resolucion, motor.resolucion)						
					}
					dibujarX +=1;
					
				}
				dibujarX = piezactual.posicionX;
				dibujarY += 1;
	}
}


//Detectamos movimiento y vemos si es un movimiento permitido o no. Básicamente tratamos de evitar que la pieza se salga del tablero, o haga otro movimiento indebido.
function detectarmovimiento(posx, posy, estadoNuevo)
{
	var resultado = true;
	var nuevaX = posx;
	var nuevaY = posy;
	for (var f = 0, longitud = motor.piezactual.estados[estadoNuevo].length; f < longitud; f++ )
	{
		for (var c = 0, longitud2 = motor.piezactual.estados[estadoNuevo][f].length; c < longitud2; c++)
		{
			if (nuevaX < 0 ||nuevaX > motor.columnas)
			{
				resultado = false;
				c = longitud2;
				f = longitud;
			}
			if (motor.tablero[nuevaY] != undefined && motor.tablero[nuevaY][nuevaX] != 0 && motor.piezactual.estados[estadoNuevo][f] != undefined && motor.piezactual.estados[estadoNuevo][f][c] != 0)
			{
				resultado = false;
				c = longitud2;
				f = longitud;
			}
			nuevaX += 1;
			
		}
		nuevaX = posx;
		nuevaY += 1;
		if (nuevaY > motor.filas)
		{
			f = longitud;
			resultado = false;
		}
	}
	return resultado;
}

//Esta función es el bucle principal del juego.
window.main = function() 
{
	
	if (motor.GameOver == true)	
	{
		motor.ctx.drawImage(motor.gameoverimagen, 0, 0, 320, 640, 0, 0, 320, 640);	
	} else 
	{
		window.requestAnimationFrame(main);
		
		motor.TiempoActual += motor.incrementotiempo;
		if (motor.TiempoActual > 500)
		{
			if (detectarmovimiento(motor.piezactual.posicionX, motor.piezactual.posicionY + 1, motor.piezactual.estadoActual))
			{
				motor.piezactual.posicionY += 1;
			}
			else
			{
				copiarDatos(motor.piezactual);
				motor.piezactual = PiezaAleatoria();
			}
			
			motor.TiempoActual = 0;
		}
		
		motor.ctx.clearRect(0,0,320,640)
		dibujarTablero();
		dibujarPieza(motor.piezactual);
	}


}




