//Este código es un cargador de conjuntos de imágenes para Javascript. Lo utilizaremos para cargar imágenes
// a nuestro Tetris de manera más sencilla, y lo ponemos en un archivo a parte por temas de organización,
//ya que no tiene que ver directamente con el juego en sí.


//Creamos una funcion / clase que llamamos CargadorImagenes, que tendrá las propiedades y métodos que 
//utilizaremos en este código.
function CargadorImagenes()
{
	//Añadimos una propiedades imagenes, un array en el que guardaremos las imágenes que queremos cargar
	this.imagenes = [];
	
	//Añadimos la propiedades imagenes_cargadas, valor que utilizaremos para saber el número de imagenes
	//que ya hemos cargado
	this.imagenes_cargadas = 0;
	
	//Añadimos la propiedad completado, un booleano para saber cuando todas las imagenes han sido cargadas
	this.completado = false;
	
	//Añadimos el metodo añadirImagen, que añadira una imagen a nuestro array para que la podamos cargar 
	//luego. Este método recibe dos parametros: La localizacion de la imagen, y el nombre.
	this.agregarImagen = function(src, nombre)
		{	
			//el metodo crea la imagen, inicializa una propiedad "cargador" a la que se asigna nuestra
			//clase, ya que usaremos sus propiedades más adelante, y finalmente crea un objeto genérico 
			//que tendrá la imagen, la fuente y su nombre, y lo coloca al final de nuestro array imagenes.
			var img = new Image();
			img.cargador = this;
			this.imagenes.push ( {imagen: img, fuente: src, nombreImg: nombre});
		};
	//Añadimos el metodo cargarImagenes, que pasará por las imagenes del array del mismo nombre y 
	//utilizará los objetos que tiene para cargar cada imagen.
	this.cargarImagenes = function()
		{
			for (var i = 0, maximo = this.imagenes.length; i < maximo; i++)
				{
					//Creamos un bucle para recorrer el array, y dentro del bucle hacemos que se asigne
					//la fuente de la imagen y su nombre, además de iniciar el proceso de cargar la
					//imagen, al que asignamos un método "callback". 
					this.imagenes[i].imagen.src = this.imagenes[i].fuente;
					this.imagenes[i].imagen.alCargar = this.alImagenCargada;
					this.imagenes[i].imagen.nombre = this.imagenes[i].nombreImg;
				}
		};
		
	//Añadimos el metodo alImagenCargada, que hemos utilizado antes como "callback" de "alCargar".
	this.alImagenCargada = function()
		{
			//El metodo incrementa la propiedad imagenes_cargadas en uno, llama a una función que
			//definiremos a continuación y mira si el numero de imagenes cargadas es igual a la longitud
			//del array. Si es así, significa que las imagenes han sido cargadas, y se ejecutan las
			//acciones pertinentes.
			this.cargador.imagenes_cargadas++;
			this.cargador.alProgresarLlamara();
			if (this.cargador.imagenes_cargadas == this.cargador.imagenes.length)
				{
					this.cargador.completado = true;
					this.cargador.alCompletarLlamara();
				}
		};
	//Creamos dos metodos para acceder a imágenes cargadas: CogerImagenEnIndice, y CogerImagenPorNombre
	this.CogerImagenEnIndice = function(indice)
		{
			//Este metodo simplemente devuelve la imagen situada en el indice que recibe.
			return this.imagenes[indice].imagen;
		};
	this.CogerImagenPorNombre = function(nombre)
		{
			//Este metodo recorre las posiciones del array imagenes y mira para cada posicion si el nombre
			//que recibe el método es el mismo del de la imagen en esa posicion. Si es así, sale del bucle
			// y devuelve dicha imagen.
			var img;
			for (var i = 0, maximo = this.imagenes.length; i < maximo; i++)
				{
					if (this.imagenes[i].nombreImg == nombre)
					{
						img = this.imagenes[i].imagen;
						i = maximo;
					}
				}
			return img;
		};
	//Añadimos el método "alCompletarLlamara" que hemos utilizado antes dentro de alImagenCargada.Este 
	//método en este archivo lo dejaremos como un error, porque estableceremos su función desde el 
	//engine del juego.
	this.alCompletarLlamara = function()
	{
			throw new Error("Caca");
	};
	//Añadimos el método alProgresarLlamara, que hemos utilizado antes dentro de alImagenCargada. Este 
	//método nos sirve para saber cuantas imagenes han sido cargadas del total. Se puede usar por ejemplo
	//para hacer una barra de "Cargando..."
	this.alProgresarLlamara = function()
	{
		var resultado;
		if (this.imagenes.length > 0)
		{
			resultado = this.imagenes_cargadas / this.imagenes.length;
		}
		else
		{
			resultado = 0;
		}
		console.log(resultado);
		return resultado;
	};
	
}