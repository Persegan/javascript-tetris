Trabajo realizado por Valent�n Lopez y Javier Jurado.

El trabajo consiste en un juego de Tetris, hecho con Javascript.
Se usan las flechas de direcci�n para jugar; las de los lados sirven para mover la pieza, la de arriba para rotarla, y la de abajo para hacer que la pieza baje m�s r�pido.

Hay algunos bugs, como que no se puede girar la pieza a veces si est� est� en el extremo derecho de la pantalla dado que la funci�n que mira si el movimiento se puede realizar 
no tiene en cuenta la posibilidad de mover la pieza una columna m�s a la izquierda, o el hecho de que a veces al girar una pieza en la esquina superior derecha de la pantalla
la pone fuera de la pantalla y coloca un game over, pero a parte de eso, los testeos que hemos realizado han mostrado tener un juego bastante s�lido.